Name:           slowmoVideo
Version:        0.5
Release:        1%{?dist}
Summary:        OpenSource program that creates slow-motion videos
License:        GPLv3
Group:          Applications/Multimedia
URL:            http://slowmovideo.granjow.net/
Source0:        https://github.com/slowmoVideo/slowmoVideo/archive/master.zip
BuildRequires:  cmake
BuildRequires:  pkgconfig
BuildRequires:  ffmpeg-devel
BuildRequires:  qt4-devel
BuildRequires:  qt5-devel
BuildRequires:  glew-devel
BuildRequires:  glut-devel
BuildRequires:  SDL-devel
BuildRequires:  pkgconfig(libpng)
BuildRequires:  pkgconfig(libjpeg)
BuildRequires:  opencv-devel
Requires:       ffmpeg
Requires:       qt
Requires:       qt5
Requires:       glew
Requires:       glut
Requires:       SDL
Requires:       pkgconfig(libpng)
Requires:       pkgconfig(libjpeg)
Requires:       opencv

%description
is an OpenSource program that creates slow-motion videos from your footage.
But it does not simply make your videos play at 0.01× speed.
You can smoothly slow down and speed up your footage,
optionally with motion blur.




%prep
%autosetup -n %{name}-master


%build
mkdir -p build
cd build
%{cmake} -D CMAKE_INSTALL_PREFIX:STRING=%{_prefix} \
	-D CMAKE_BUILD_TYPE:STRING=Release ../src
%make_build


%install
rm -rf $RPM_BUILD_ROOT
%make_install -C build


%files
%license LICENSE.md
%doc README.md
%{_bindir}/*
%{_datadir}/applications/slowmoUI.desktop
%{_datadir}/icons/AppIcon.png



%changelog
* Thu May 31 2018 yusuf sourani <youssef.m.sourani@gmail.com> 0.5-1
- Initial For Fedora 28
